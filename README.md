# README #

Sistema para el control y asignacion de ubicaciones de los funcionarios del SATDC (Servicio de Administracion Tributaria del Distrito Capital), en los diferentes departamentos de la institucion y Registros mercantiles.


**Este sistema fue Desarrollado con:**
1. PHP 5.5, (**Framework Codeigniter ******).
2.  MySQL 5.5.
3. Bootstrap 3.3.

Actualmente en uso en la intranet de la institucion

![ubicaciones.jpg](https://bitbucket.org/repo/6dMzb7/images/2287744559-ubicaciones.jpg)