
<table class="table table-hover table-striped">
<thead>
  <tr>
  	<th>#</th>
    <th>user id</th>
  	<th>Login user</th>
  	<th>Nombre Funcionario</th>
  	<th>Ubicacion Actual</th>
  </tr>
</thead>
<tbody>
	
			<?php 
			
			$cont=0;
			foreach ($data as $data_item):
			$cont++;?>

		<tr id= "<?php echo "tr_".$cont?>">
				<td><?php echo $cont?></td>
                <td id="<?php echo "user_".$cont; ?>"><?php echo $data_item['usuario_id']?></td>
				<td><?php echo $data_item['login']?></td>
				<td><?php echo  $data_item['nombre1'].' '.$data_item['apellido1']?></td>
				<td>
				<div class="row">
					<div class="col-md-8">
				<select class="form-control" id="<?php echo "select_".$cont; ?>">
					<?php foreach ($descripcion_unidad as $descripcion_unidad_item):
							$seleccion='';
							if($descripcion_unidad_item['id_unidad']==$data_item['id_unidad']) $seleccion='selected';
							echo "<option value='".$descripcion_unidad_item['id_unidad']."'$seleccion>".$descripcion_unidad_item['descripcion']."</option>";
							
                        endforeach;?>
					<?php if ($data_item['id_unidad']=='') echo "<option value=1000 selected>Sin ubicacion</option>"; ?>
				</select>
               
			</div>
            <div class="col-md-1 col-sm-2">
             <button type="button" class="btn btn-danger btn-xs" <?php echo "onclick= modificar($cont) " ?>><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Cambiar</button>
            </div>
		</div>
				</td>
				

			</tr>
		<?php endforeach;?>
</tbody>
</table>
