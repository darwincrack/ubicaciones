<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo $title ?></title>

    <!-- Bootstrap Core CSS -->
<link rel="stylesheet" href="<?php echo  base_url()?>css/bootstrap.min.css">

    <!-- Custom CSS -->
<link rel="stylesheet" href="<?php echo  base_url()?>css/sb-admin-2.css">


    <!-- Custom Fonts -->
<link rel="stylesheet" href="<?php echo  base_url()?>font_awesome-4.1.0/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo  base_url()?>css/plugins/metisMenu/metisMenu.min.css">

    <!-- jQuery -->
	<script src="<?php echo  base_url()?>js/jquery.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<style>

@media screen and (max-width: 768px) {
  body { padding-top: 63px; }
}
a{
	color:#E83135;
}
.barra{
	background-image:url(img/bg.png);
	background-color:#FF1C1F;
}
</style>
</head>

<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-fixed-top barra" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
            
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" style="color:#FFFFFF; font-weight:bold; padding:0" href="<?php echo base_url().'control_usuarios'  ?>"><img src="<?php echo base_url().'img/LOGO4.png'  ?>" style="width: 251px; height:50px; background:#F8F8F8;"  alt=""></a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">

                <!-- /.dropdown -->
                <li class="dropdown">
                    <a style="color:#FFFFFF;" class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                       <!-- <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>-->
                        <li><a  href="<?php echo base_url().'login/logout'  ?>"><i class="fa fa-sign-out fa-fw"></i> Cerrar Sesion</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
						 <li >
                            <a href="<?php echo base_url().'control_usuarios'  ?>" class="active"><i class="fa fa-edit fa-fw"></i> Ubicar Funcionarios</a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
 
            <!-- /.row -->
            <!--titulo-->
       
               <div class="row">
                <div class="col-lg-12 col-sm-8">
				<!--titulo de cada pagina-->
                    <h1 class="page-header"><?php echo $title ?></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
            
            
            
            
            <!--fin del titulo-->
            
            

            
            
            
            
            
            <div class="row">
			<!--contenido-->
            
			<?php echo $content ?>
			<!--fin contenido-->
            </div>
            <!-- /.row -->
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->


	

    <!-- Bootstrap Core JavaScript -->
	<script src="<?php echo  base_url()?>js/bootstrap.min.js"></script>
    
   
     <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo  base_url()?>js/plugins/metisMenu/metisMenu.min.js"></script>  
   
    <!-- Custom Theme JavaScript -->
	<script src="<?php echo  base_url()?>js/sb-admin-2.js"></script>
    <script src="<?php echo  base_url()?>js/bootbox.js"></script>
    <script src="<?php echo  base_url()?>js/funciones.js"></script>
    
    


</body>
</html>




