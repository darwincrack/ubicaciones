<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {
	
	function __construct()
    {
       parent::__construct();
	   $this->load->model('login_model');
	   $this->load->library('encrypt');
	   $this->load->helper('cookie');
	   $this->load->helper('url');
    }
	
	function index()
	{

		if ($this->session->userdata('is_logued_in') ===FALSE)
		{
			$data['token'] = $this->_token();
			$this->load->view('login_view',$data);
		}
		else
		{

				redirect(base_url().'control_usuarios');

			
			
		}
	}
	
	
	
	public function user_login()
	{	$this->output->enable_profiler(TRUE);
		if($this->input->post('token',true) && $this->input->post('token',true) == $this->session->userdata('token'))
		{
        	$this->form_validation->set_rules('usuario', 'Usuario', 'required|trim|min_length[2]|max_length[20]|xss_clean');
            $this->form_validation->set_rules('contrasena', 'Contraseña', 'required|trim|min_length[3]|max_length[20]|xss_clean');
			$this->form_validation->set_message('required', ' %s No puede estar en blanco');
			$this->form_validation->set_message('min_length', ' El campo %s debe ser al menos de %s caracteres');
			$this->form_validation->set_message('max_length', ' El campo %s no puede exceder de %s caracteres');
			$this->form_validation->set_error_delimiters('<p class="bg-danger">','</p>');
            //lanzamos mensajes de error si es que los hay
            
			if($this->form_validation->run() === FALSE)
			{
				$this->index();
			}else{
				$check_user = $this->login_model->get_login_user();
				if($check_user == TRUE)
				{
					$data = array(
	                'is_logued_in' 		=> 		TRUE,
	                'user_login'		=>		$check_user->login,
					'area_id'			=>		$check_user->area_id
            		);		
					$this->session->set_userdata($data);
					$this->index();
				}
			}
		}else{
			redirect(base_url().'login');
		}
	}
	
	
		public function logout()
	{
		$this->session->sess_destroy();
		delete_cookie("men_corresp");
		redirect(base_url().'login');
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public function _token()
	{
		$token = md5(uniqid(rand(),true));
		$this->session->set_userdata('token',$token);
		return $token;
	}
	

}