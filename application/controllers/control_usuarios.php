<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Control_usuarios extends CI_Controller {
	function __construct()
    {
       parent::__construct();
	   $this->load->helper('url');
    }
	
	public function index()
	{
		   if($this->session->userdata('is_logued_in')===FALSE) redirect(base_url().'/login/logout');
		   $this->load->helper('url');
			$this->template->write('title', 'Ubicacion de Funcionarios', TRUE);
			$this->template->write_view('content','control_usuarios');
			$this->template->render();
		
	}
	
	public function control_user_informatica()
	{
		if($this->session->userdata('is_logued_in')===FALSE) redirect(base_url().'/login/logout');
		
		   $this->load->helper('url');
			$this->template->write('title', 'Ubicacion de Funcionarios', TRUE);
			$this->template->write_view('content','control_usuarios');
			$this->template->render();
		
	}

	public function search_user()
	{
		if($this->session->userdata('is_logued_in')===FALSE) redirect(base_url().'/login/logout');
		
		// $this->output->enable_profiler(TRUE);
		$this->load->model('control_usuarios_model');
		$nombre_usuario=$this->input->post('name_user');
		$data['descripcion_unidad']=$this->control_usuarios_model->get_descripcion_areas();
		$data['data']=$this->control_usuarios_model->get_usuarios($nombre_usuario);
		
			if(empty($data['data']))
			{ 
				$this->load->view('no_encontrado',$data);
				
			}
			else
			{
				if($this->session->userdata('area_id')==5)
				{
					$this->load->view('table_informatica',$data);
				}
				else
				{
					$this->load->view('table',$data);
				}	
			}
		
		


	}


	function update_usuarios()
	{
		if($this->session->userdata('is_logued_in')===FALSE) redirect(base_url().'/login/logout');
		
		$this->output->enable_profiler(TRUE);
		$this->load->model('control_usuarios_model');
		$id_ubicacion=$this->input->post('id_ubicacion');
		$id_usuario=$this->input->post('id_usuario');
		$this->control_usuarios_model->update_ubicacion($id_usuario,$id_ubicacion);	
	}

	function reset_password()
	{
		if($this->session->userdata('is_logued_in')===FALSE) redirect(base_url().'/login/logout');
		
		$this->load->model('control_usuarios_model');
		$id_usuario=$this->input->post('id_usuario');
		$pass=$this->input->post('pass');
		if($pass=='reset' or $pass=='')
		{
			$pass='12345';
		}
		echo $this->control_usuarios_model->reset_password($id_usuario,$pass);	

	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */