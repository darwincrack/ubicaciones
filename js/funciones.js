$(document).ready(function(){ //inicio jquery

   $("input").bind("keydown", function(event) {
      // track enter key
      var keycode = (event.keyCode ? event.keyCode : (event.which ? event.which : event.charCode));
      if (keycode == 13) { // keycode for enter key
         // force the 'Enter Key' to implicitly click the Update button
		 $("#btn_buscar").click();
         return false;
      } else  {
         return true;
      }
   }); // end of function



//buscar el usuario o registro mercantil
$("#btn_buscar").click(function(){

	
	var parametros = {
	"name_user" : $('#name_user').val()
	};
	$.ajax({
	data:  parametros,
	url:   'control_usuarios/search_user',
	type:  'post',
	 /*  beforeSend: function () {
				$("#resultado").html("Procesando, espere por favor...");
		},*/
	success:  function (data) {
		
			$('#table_cont').html(data);
	},
	error: function(xhr, status, error) {
			alert("ocurrio un "+status+":"+error);
	}
	});
	
	
	/*  $.post("control_usuarios/search_user",
	{
	name_user : $('#name_user').val(),
	},
	function(data) {
	  $('#table_cont').html(data);
	
	});*/
}); 
});//fin jquery

//cambiar ubicacion del usuario
 function modificar(id)
 {
	bootbox.confirm({ 
	title:'Elige la Opción',
	animate:true,
	message: "Seguro desea cambiar de ubicacion a este usuario??",
	callback: function(result){
		if(result==true)
		{
			var parametros = {
			"id_usuario" : document.getElementById("user_"+id).innerHTML,
			"id_ubicacion" : $('#select_'+id+' :selected').val()
			};
		
			$.ajax({
			data:  parametros,
			url:   'control_usuarios/update_usuarios',
			type:  'post',
		  /*  beforeSend: function () {
					$("#resultado").html("Procesando, espere por favor...");
			},*/
			success:  function (data) {
				
				   $("#tr_"+id).attr("class","success");
				    setTimeout(function(){ $("#tr_"+id).removeClass("success"); }, 3000);
			},
			error: function(xhr, status, error) {
				bootbox.alert({
					title:'Error',
					message: "Inesperado "+status+": "+error
				});
			}
		});
			  /*$.post("control_usuarios/update_usuarios",
			  {
			  id_usuario:document.getElementById("user_"+id).innerHTML,
			  id_ubicacion:$('#select_'+id+' :selected').val()
			  },
			  function(data) {
			  
				$("#tr_"+id).attr("class","success");
			  
			  });*/
		} 		
	}
	})
}

//modificar contraseña
function modificar_pass(id)
{
	bootbox.confirm({ 
  title:'Elige la Opción',
  message: "Desea Cambiar la Clave??",
  callback: function(result){
	  if(result==true)
	  {
		  var parametros = {
		  "id_usuario":document.getElementById("user_"+id).innerHTML,
		  "pass":document.getElementById("pass_"+id).value
		  };
	  
		  $.ajax({
		  data:  parametros,
		  url:   'control_usuarios/reset_password',
		  type:  'post',
		/*  beforeSend: function () {
				  $("#resultado").html("Procesando, espere por favor...");
		  },*/
		  success:  function (data) {
			  $("#tr_"+id).attr("class","warning");	
			  
			  
			  setTimeout(function(){ $("#tr_"+id).removeClass("warning"); }, 3000);
			     
		  },
		  error: function(xhr, status, error) {
			  // handle error	
			  bootbox.alert({
				  title:'Error',
				  message: "Inesperado "+status+": "+error+" , Intente de nuevo."
			  });
		  }
	  	});
	  } 		
  	}
  }) 
   
  /*var r=confirm('Desea Cambiar la Clave??');
  if(r==true)
  {
	  $.post("control_usuarios/reset_password",
	 {
	  id_usuario:document.getElementById("user_"+id).innerHTML,
	  pass:document.getElementById("pass_"+id).value
	  },
	  function(data) {
	  if(data ==1 || data==0){
		$("#tr_"+id).attr("class","warning");
	  }
	  else
	  {
		  alert("no se pudo cambiar la contraseña, intente de nuevo");
	  }
	  });
  }*/
}